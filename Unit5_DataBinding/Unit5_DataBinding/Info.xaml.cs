﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Unit5.Core;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace Unit5_DataBinding
{
    public class ImageNameConvert : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            string imageName = value.ToString();
            return ImageSource.FromResource($"Unit5_DataBinding.{imageName}");
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class Info : ContentPage
    {
        private void Button_Clicked(object s, EventArgs e1)
        {
            ((InfoViewModel)this.BindingContext).Bonus = 100;
        }


        // private InfoData data;
        public Info()
        {
           // data = new InfoData();
            //this.BindingContext = data;
            InitializeComponent();
            //return;
            //this.imgMe.SetBinding(
            //    Image.SourceProperty, 
            //    nameof(data.ImagePath), 
            //    converter: new ImageNameConvert());
            

            //txtName.BindingContext = data;
            //txtName.SetBinding(
            //    Entry.TextProperty,nameof(data.Name),
            //    mode:BindingMode.TwoWay );

            //Binding nameBinding = new Binding();
            //nameBinding.Source = data;
            //nameBinding.Path = nameof(data.Name);
            //this.txtName.SetBinding(Entry.TextProperty, nameBinding);

            //this.txtName.Text = data.Name;
            //this.txtEmail.Text = data.Email;
            //this.pickGander.SelectedIndex = (int)data.Gander;
            //this.dateBirth.Date = data.Birthday;
            //this.imgMe.Source = ImageSource.FromResource($"Unit5_DataBinding.{data.ImagePath}");
        }


    }
}
